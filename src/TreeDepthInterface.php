<?php
namespace Joberate;

interface TreeDepthInterface
{
    /**
     * @param AbstractTree $tree
     * @return int
     */
    public function getTreeDepth(AbstractTree $tree);
}
