<?php
namespace Joberate;

class Tree extends AbstractTree
{
    /**
     * @param AbstractTree $tree
     * @return int
     */
    public function getTreeDepth(AbstractTree $tree)
    {
        throw new \RuntimeException('Implement me');
    }
}
