<?php
namespace Joberate;

abstract class AbstractTree implements TreeDepthInterface
{
    /**
     * @var AbstractTree|null
     */
    private $left;

    /**
     * @var AbstractTree|null
     */
    private $right;

    /**
     * @param AbstractTree|null $left
     * @param AbstractTree|null $right
     */
    public function __construct(AbstractTree $left = null, AbstractTree $right = null)
    {
        $this->left = $left;
        $this->right = $right;
    }

    /**
     * @return AbstractTree|null
     */
    public function getLeft()
    {
        return $this->left;
    }

    /**
     * @return AbstractTree|null
     */
    public function getRight()
    {
        return $this->right;
    }
}
