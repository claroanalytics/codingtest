<?php
namespace Joberate;

class TreeTest extends \PHPUnit_Framework_TestCase
{
    private function buildTree()
    {
        $tree9 = new Tree();
        $tree8 = new Tree(null, $tree9);
        $tree7 = new Tree($tree8, null);
        $tree6 = new Tree();
        $tree5 = new Tree();
        $tree4 = new Tree($tree7, null);
        $tree3 = new Tree(null, $tree6);
        $tree2 = new Tree($tree5, null);
        $tree1 = new Tree($tree3, $tree4);
        $tree = new Tree($tree1, $tree2);

        return $tree;
    }

    public function testTreeDepth()
    {
        $tree = $this->buildTree();
        $this->assertEquals(6, $tree->getTreeDepth($tree));
    }
}
